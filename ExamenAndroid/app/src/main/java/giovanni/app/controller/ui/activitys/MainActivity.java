package giovanni.app.controller.ui.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.mikelau.views.shimmer.ShimmerRecyclerViewX;

import java.util.List;

import giovanni.app.controller.ui.adapter.MoviesAdapter;
import giovanni.app.examenandroid.R;
import giovanni.app.services.ApiClient;
import giovanni.app.services.responses.MovieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ShimmerRecyclerViewX shimmerRecycler;
    MoviesAdapter moviesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        getMovieServices();
    }

    private void init() {
        shimmerRecycler = findViewById(R.id.shimmer_recycler_view);

        shimmerRecycler.setLayoutManager(new LinearLayoutManager(this));
        shimmerRecycler.setAdapter(moviesAdapter);
        shimmerRecycler.showShimmerAdapter();
    }

    private void getMovieServices(){

        Call<List<MovieResponse>> userlist = ApiClient.getMovieServices().GetMovie();

        userlist.enqueue(new Callback<List<MovieResponse>>() {

            @Override
            public void onResponse(Call<List<MovieResponse>> call, Response<List<MovieResponse>> response) {

                if(response.isSuccessful()){
                    List<MovieResponse> movieResponses = response.body();
                    moviesAdapter.setData(movieResponses);
                    shimmerRecycler.setAdapter(moviesAdapter);
                    shimmerRecycler.hideShimmerAdapter();
                }

            }

            @Override
            public void onFailure(Call<List<MovieResponse>> call, Throwable t) {
                Log.e("failure",t.getLocalizedMessage());


            }
        });
    }
}