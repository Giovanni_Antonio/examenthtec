package giovanni.app.controller.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import giovanni.app.examenandroid.R;
import giovanni.app.services.responses.MovieResponse;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesAdapterVH> {

    private List<MovieResponse> movieResponses;
    private Context context;
    private ClickedItem clickedItem;


    public MoviesAdapter(ClickedItem clickedItem) {
        this.clickedItem = clickedItem;

    }

    public void setData(List<MovieResponse> movieResponses){
        this.movieResponses = movieResponses;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MoviesAdapterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new MoviesAdapter.MoviesAdapterVH(LayoutInflater.from(context).inflate(R.layout.row_movie,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesAdapterVH holder, int position) {
        MovieResponse movieResponse = movieResponses.get(position);
        String title = movieResponse.getTitle();
        String originalLenguaje = movieResponse.getOriginal_language();
        String vote = movieResponse.getVote_count();

        holder.cardTitle.setText(title);
        holder.cardLenguajeOriginal.setText(originalLenguaje);
        holder.cardVote.setText(vote);
        Picasso.get().load(movieResponses.get(position).getPoster_path()).into(holder.imageView);
        holder.imageView.setOnClickListener(view -> clickedItem.ClickedUser(movieResponse));
    }

    public interface  ClickedItem{
        void ClickedUser(MovieResponse movieResponse);
    }

    @Override
    public int getItemCount() {
        return movieResponses.size();
    }

    public class MoviesAdapterVH extends RecyclerView.ViewHolder {
        TextView cardTitle;
        TextView cardLenguajeOriginal;
        TextView cardVote;
        ImageView imageView;

        public MoviesAdapterVH(@NonNull View itemView) {
            super(itemView);

            cardTitle = itemView.findViewById(R.id.txtMovie);
            cardLenguajeOriginal = itemView.findViewById(R.id.textLenguajeOriginal);
            cardVote = itemView.findViewById(R.id.textVoto);
            imageView = itemView.findViewById(R.id.image_View_movie);
        }
    }

}
