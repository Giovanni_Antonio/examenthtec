package giovanni.app.services.interfaces;

import java.util.List;

import giovanni.app.services.responses.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Movies {
    @GET("top_rated?api_key=944babc69e472b7a765206267feeb43a")
    Call<List<MovieResponse>> GetMovie();
}
